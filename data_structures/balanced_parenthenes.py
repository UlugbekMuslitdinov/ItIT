def balanced_parentheses(string):
    """
    Check if a string has balanced parentheses.
    """
    stack = []
    for char in string:
        if char == '(':
            stack.append(char)
        elif char == ')':
            if len(stack) == 0:
                return False
            stack.pop()
    return len(stack) == 0