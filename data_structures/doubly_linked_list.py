# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 12:32:30 2018

@author: Desmond
"""


class Node(object):
    # Each node has its data and a pointer that points to next_node node in the Linked List
    def __init__(self, data, next_node=None, previous=None):
        self.data = data
        self.next = next_node
        self.previous = previous


class DoublyLinkedList(object):
    def __init__(self):
        self.head = None

    # for inserting at beginning of linked list
    def insertAtStart(self, data):
        if self.head is None:
            new_node = Node(data)
            self.head = new_node
        else:
            new_node = Node(data)
            self.head.previous = new_node
            new_node.next = self.head
            self.head = new_node

    # for inserting at end of linked list
    def insertAtEnd(self, data):
        new_node = Node(data)
        temp = self.head
        while temp.next is not None:
            temp = temp.next
        temp.next = new_node
        new_node.previous = temp

    # deleting a node from linked list
    def delete(self, data):
        temp = self.head
        if temp.next is not None:
            # if head node is to be deleted
            if temp.data == data:
                temp.next.previous = None
                self.head = temp.next
                temp.next = None
                return
            else:
                while temp.next is not None:
                    if temp.data == data:
                        break
                    temp = temp.next
                if temp.next:
                    # if element to be deleted is in between
                    temp.previous.next = temp.next
                    temp.next.previous = temp.previous
                    temp.next = None
                    temp.previous = None
                else:
                    # if element to be deleted is the last element
                    temp.previous.next = None
                    temp.previous = None
                return

        if temp is None:
            return

    # for printing the contents of linked lists
    def printdll(self):
        temp = self.head
        while temp is not None:
            print('%d ' % temp.data)
            temp = temp.next

    def reverse(self):
        temp = self.head
        while temp is not None:
            temp.previous, temp.next = temp.next, temp.previous
            temp = temp.previous
        self.head = temp.next
        temp.next = None
        return self.head

    def delete(self, i):
        temp = self.head
        if temp.next is not None:
            # if head node is to be deleted
            if temp.data == i:
                temp.next.previous = None
                self.head = temp.next
                temp.next = None
                return
            else:
                while temp.next is not None:
                    if temp.data == i:
                        break
                    temp = temp.next
                if temp.next:
                    # if element to be deleted is in between
                    temp.previous.next = temp.next
                    temp.next.previous = temp.previous
                    temp.next = None
                    temp.previous = None
                else:
                    # if element to be deleted is the last element
                    temp.previous.next = None
                    temp.previous = None
                    return


dll = DoublyLinkedList()
dll.insertAtStart(1)
dll.insertAtStart(2)
dll.insertAtEnd(3)
dll.insertAtStart(4)
dll.printdll()
dll.delete(2)
print()
dll.printdll()
