# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 12:28:09 2018

@author: Desmond
"""


# Linked List and Node can be accommodated in separate classes for convenience

class Node(object):
    # Each node has its data and a pointer that points to next_node node in the Linked List
    def __init__(self, data, next_node=None):
        self.data = data
        self.next = next_node

    # function to set data
    def setData(self, data):
        self.data = data

    # function to get data of a particular node
    def getData(self):
        return self.data

    # function to set next_node node
    def setNext(self, next_node):
        self.next = next_node

    # function to get the next_node node
    def getNext(self):
        return self.next


def insertBetween(previous_node, data):
    if previous_node.next is None:
        print('Previous node should have next_node node!')
    else:
        new_node = Node(data)
        new_node.next = previous_node.next
        previous_node.next = new_node


class LinkedList(object):
    # Defining the head of the linked list
    def __init__(self):
        self.head = None

    # printing the data in the linked list
    def printLinkedList(self):
        temp = self.head
        while temp:
            print('%d ' % temp.data)
            temp = temp.next

    # inserting the node at the beginning
    def insertAtStart(self, data):
        new_node = Node(data)
        new_node.next = self.head
        self.head = new_node

    # inserting the node in between the linked list (after a specific node)

    # inserting at the end of linked list
    def insertAtEnd(self, data):
        new_node = Node(data)
        temp = self.head
        while temp.next is not None:  # get last node
            temp = temp.next
        temp.next = new_node

    # deleting an item based on data(or key)
    def delete(self, data):
        global prev
        temp = self.head
        # if data/key is found in head node itself
        if temp.next is not None:
            if temp.data == data:
                self.head = temp.next
                temp = None
                return
            else:
                #  else search all the nodes
                while temp.next is not None:
                    if temp.data == data:
                        break
                    prev: object = temp  # save current node as previous so that we can go on to next_node node
                    temp = temp.next

                # node not found
                if temp is None:
                    return

                prev.next = temp.next
                return

    # iterative search
    def search(self, node, data):
        if node is None:
            return False
        if node.data == data:
            return True
        return self.search(node.getNext(), data)

    def length(self):
        temp = self.head
        count = 0
        while temp:
            count += 1
            temp = temp.next
        return count

    def index(self, i):
        temp = self.head
        count = 0
        while temp:
            if count == i:
                return temp.data
            count += 1
            temp = temp.next
        return None




if __name__ == '__main__':
    List = LinkedList()
    List.head = Node(1)  # create the head node
    node2 = Node(2)
    List.head.setNext(node2)  # head node's next_node --> node2
    node3 = Node(3)
    node2.setNext(node3)  # node2's next_node --> node3
    List.insertAtStart(4)  # node4's next_node --> head-node --> node2 --> node3
    insertBetween(node2, 5)  # node2's next_node --> node5
    List.insertAtEnd(6)
    List.printLinkedList()
    print()
    List.delete(3)
    List.printLinkedList()
    print()
    print(List.search(List.head, 1))
