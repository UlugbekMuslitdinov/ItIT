# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 14:28:49 2018

@author: Desmond
"""
from __future__ import print_function


class TreeNode(object):
    def __init__(self, data=None):
        self.left = None
        self.right = None
        self.data = data

    # for setting left node
    def setLeft(self, node):
        self.left = node

    # for setting right node
    def setRight(self, node):
        self.right = node

    # for getting the left node
    def getLeft(self):
        return self.left

    # for getting right node
    def getRight(self):
        return self.right

    # for setting data of a node
    def setData(self, data):
        self.data = data

    # for getting data of a node
    def getData(self):
        return self.data

    def insert(self, val):
        if self is None:
            self = TreeNode(val)
        elif val < self.data:
            self.left = TreeNode.insert(self.left, val)
        else:
            self.right = TreeNode.insert(self.right, val)
        return self

    def bin_search(self, targetValue):
        if self is None: return False
        testEntry = self.data
        if targetValue == testEntry:
            return True
        if targetValue < testEntry:
            return TreeNode.bin_search(self.left, targetValue)
        if targetValue > testEntry:
            return TreeNode.bin_search(self.right, targetValue)

    # in this we traverse first to the leftmost node, 
    # then print its data and then traverse for rightmost node
    def inorder(self):
        if self:
            TreeNode.inorder(self.getLeft())
            print(self.getData(), end=' ')
            TreeNode.inorder(self.getRight())
        return

    # in this we first print the root node and 
    # then traverse towards leftmost node and then to the rightmost node
    def preorder(self):
        if self:
            print(self.getData(), end=' ')
            TreeNode.preorder(self.getLeft())
            TreeNode.preorder(self.getRight())
        return

        # in this we first traverse to the leftmost node and

    # then to the rightmost node and then print the data
    def postorder(self):
        if self:
            TreeNode.postorder(self.getLeft())
            TreeNode.postorder(self.getRight())
            print(self.getData(), end=' ')
        return

    def depth(self):
        if self is None: return 0
        return 1 + max(TreeNode.depth(self.getLeft()), TreeNode.depth(self.getRight()))

    def count(self):
        if self is None: return 0
        return 1 + TreeNode.count(self.getLeft()) + TreeNode.count(self.getRight())

    def stack_inorder(self):
        s = []
        while True:
            while self:
                s.append(self)
                self = self.getLeft()
            if not s:
                break
            self = s.pop()
            print(self.getData(), end=' ')
            self = self.getRight()
        return


root = TreeNode(1)
root.setLeft(TreeNode(2))
root.setRight(TreeNode(3))
root.left.setLeft(TreeNode(4))
root.left.setRight(TreeNode(5))
root.right.setLeft(TreeNode(6))
root.right.setRight(TreeNode(7))

''' This will a create like this 
                    1
                /       \
            2            3
        /      \      /     \
    4            5   6       7
'''

print('Inorder:')
TreeNode.inorder(root)
print('\nPreorder:')
TreeNode.preorder(root)
print('\nPostorder: ')
TreeNode.postorder(root)
